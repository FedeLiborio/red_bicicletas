var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
};

exports.bicicleta_create = function(req, res){
    var bici = new Bicicleta();
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);


    res.status(200).json({
        bicicleta: bici
    });
};

exports.bicicleta_update = function(req, res){
    var bici = Bicicleta.findById(req.body.id);
    if(req.body.id){
        bici.id = req.body.id;
    }

    if(req.body.color){
        bici.color = req.body.color;
    }
    
    if(req.body.modelo){
        bici.modelo = req.body.modelo;
    }
    
    if(req.body.lat && req.body.lng){
        bici.ubicacion = [req.body.lat, req.body.lng];
    }
    
    res.status(200).json({
        bicicleta: bici
    });
};

exports.bicicleta_delete = function(req, res){
    Bicicleta.removeById(req.body.id);

    res.status(204).send();
}